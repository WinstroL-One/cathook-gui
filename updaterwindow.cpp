#include "updaterwindow.h"
#include "binarymodewindow.h"
#include "sourcemodewindow.h"
#include "ui_updaterwindow.h"
#include <QApplication>
#include <QFile>
#include <QMessageBox>
#include <QProcess>
#include <QPropertyAnimation>
#include <QSettings>
#include <QSysInfo>
#include <iostream>

void updaterwindow::doUpdate() {
  ui->progressBar->setValue(100);
  QMessageBox::StandardButton reply = QMessageBox::question(
      this, "Update", "Cathook-GUI update found! Do you want to update?",
      QMessageBox::Yes | QMessageBox::No);
  if (reply == QMessageBox::Yes) {
    ui->label->setText("Updating...");
    ui->progressBar->setValue(0);
    auto updater = new QProcess();
    updater->setProcessChannelMode(QProcess::MergedChannels);
    updater->setWorkingDirectory(qApp->applicationDirPath() +
                                 "/appimageupdatetool");
    updater->start("./AppRun", {"-O", "-r", qgetenv("APPIMAGE")});

    if (updater->waitForStarted()) {
      connect(updater, &QProcess::readyReadStandardOutput, [=]() {
        QString data = updater->readAllStandardOutput();
        std::cout << data.toStdString() << std::endl;
        bool ok;
        int percent = data.remove(0, 5).split("%")[0].toFloat(&ok);
        if (ok)
          ui->progressBar->setValue(percent);
      });
      connect(updater,
              QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished),
              [=](int exitCode) {
                if (exitCode != 0)
                  QMessageBox::critical(this, "Update failed!",
                                        "Update failed!");
                updater->deleteLater();
                // Restart the program
                QProcess restart;
                restart.startDetached(qgetenv("APPIMAGE"));
                qApp->quit();
              });
    } else {
      updater->kill();
      updater->deleteLater();
    }
  } else {
    if (QFile::exists("/opt/cathook/os/")) {
      cathookOS();
    } else
      startWindow();
  }
}

updaterwindow::updaterwindow(QWidget *parent)
    : QDialog(parent), ui(new Ui::updaterwindow) {
  ui->setupUi(this);
  ui->label->setText("Checking for updates...");
  ui->progressBar->setValue(0);

  auto updatecheck = new QProcess();
  updatecheck->setWorkingDirectory(qApp->applicationDirPath() +
                                   "/appimageupdatetool");
  updatecheck->start("./AppRun", {"-j", qgetenv("APPIMAGE")});
  if (updatecheck->waitForStarted(2000)) {
    this->show();
    auto animation = new QPropertyAnimation(ui->progressBar, "value");
    animation->setDuration(8000);
    animation->setStartValue(0);
    animation->setEndValue(100);
    animation->start();
    connect(animation, &QPropertyAnimation::finished,
            [=]() { updatecheck->kill(); });

    connect(updatecheck,
            QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished),
            [=](int exitCode, QProcess::ExitStatus exitStatus) {
              animation->stop();
              animation->deleteLater();
              updatecheck->deleteLater();
              if (exitCode == 1 && exitStatus == QProcess::NormalExit)
                doUpdate();
              else {
                if (QFile::exists("/opt/cathook/os/")) {
                  cathookOS();
                } else
                  startWindow();
              }
            });
  } else {
    updatecheck->kill();
    delete updatecheck;
    startWindow();
  }
}

void updaterwindow::startWindow() {
  bool binary = false;
  bool source = false;
  bool archlinux = false;

  QString type = QSysInfo::productType();

  if (type == "arch" || type == "manjaro" || type == "garuda") {
    archlinux = true;
  }

  QSettings settings("settings");

  switch (settings.value("cathook-gui/mode", 0).toInt()) {
  case 1: {
    QMessageBox msgBox;
    msgBox.setText("Launch Cathook GUI binary or source mode?");
    QPushButton *button_binary =
        msgBox.addButton(tr("Binary mode"), QMessageBox::NoRole);
    QPushButton *button_source =
        msgBox.addButton(tr("Source mode"), QMessageBox::NoRole);
    msgBox.exec();
    auto clicked_button = (QPushButton *)msgBox.clickedButton();

    if (clicked_button == button_binary)
      binary = true;
    else if (clicked_button == button_source)
      source = true;
    break;
  }
  case 2:
    binary = true;
    break;
  case 3:
    source = true;
    break;
  case 0:
  default:
    if (type == "fedora" || archlinux)
      source = true;
    else if (type == "ubuntu") {
      bool ok;
      int major = QSysInfo::productVersion().left(2).toInt(&ok);
      if (!ok || major < 20)
        binary = true;
      else
        source = true;
    } else
      binary = true;
  }

  if (binary) {
    window_binarymode = new BinarymodeWindow(nullptr, archlinux);
    window_binarymode->show();
  } else if (source) {
    window_sourcemode = new SourcemodeWindow();
    window_sourcemode->show();
  } else
    qApp->quit();
  this->hide();
}

void updaterwindow::cathookOS() {
  QSettings settings("settings");
  if (!QFile::exists("cathook")) {
    switch (settings.value("cathookos/version", 0).toInt()) {
    case 0: {
      ui->label->setText("Checking for OS updates...");
      auto checkupdates = new QProcess();
      checkupdates->start("checkupdates");

      auto animation = new QPropertyAnimation(ui->progressBar, "value");
      animation->setDuration(30000);
      animation->setStartValue(0);
      animation->setEndValue(100);
      animation->start();
      connect(animation, &QPropertyAnimation::finished,
              [=]() { checkupdates->kill(); });

      connect(checkupdates,
              QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished),
              [=](int exitCode, QProcess::ExitStatus exitStatus) {
                QSettings settings("settings");
                ui->progressBar->setValue(100);
                animation->stop();
                animation->deleteLater();
                checkupdates->deleteLater();

                bool error = false;

                if (exitStatus == QProcess::NormalExit) {
                  switch (exitCode) {
                    // Update needed
                  case 0:
                      QMessageBox::information(
                          this, "OS upgrade required",
                          "CathookOS needs to be updated before Cathook can be used.");
                    QProcess::startDetached("pamac-manager --updates");
                    qApp->quit();
                    return;
                    // Error
                  case 1:
                    error = true;
                  case 2:
                    // No update
                    settings.setValue("cathookos/version", 1);
                    break;
                  }
                } else
                  error = true;
                if (error) {
                  settings.setValue("cathookos/version", 1);
                  QMessageBox::information(
                      this, "Update check failed.",
                      "Please make sure your system is fully up "
                      "to date before you install cathook.");
                  // Don't do this, I'm literally just too lazy to use
                  // QDesktopServices...
                  system("xdg-open "
                         "https://wiki.manjaro.org/"
                         "index.php?title=Pacman_Overview#Installing_Updates");
                }
                startWindow();
              });
      return;
    }
    default:
      break;
    }
  } else {
    settings.setValue("cathookos/version", 1);
  }
  startWindow();
}

updaterwindow::~updaterwindow() {
  delete ui;
  // if (window_binarymode)
  //  delete window_binarymode;
  if (window_sourcemode)
    delete window_sourcemode;
}
