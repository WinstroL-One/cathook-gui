#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTextEdit>
#include <memory>

class QProcess;

namespace Ui {
class MainWindow;
}

class SourcemodeWindow : public QMainWindow {
  Q_OBJECT

public:
  explicit SourcemodeWindow(QWidget *parent = nullptr);
  ~SourcemodeWindow();

private slots:
  void printTerminal(QString data);
  void readTerminalOutput();
  void closeTerminalProcess();

  void on_injectButton_clicked();
  void on_configButton_clicked();
  void on_preloadButton_clicked();
  void on_updateButton_clicked();
  void on_actionSelect_Maps_triggered();
  void on_actionSettings_triggered();

private:
  Ui::MainWindow *ui;
  std::unique_ptr<QProcess> process;
  void setTextTermFormatting(QTextEdit *textEdit, QString const &text);
  void parseEscapeSequence(int attribute, QListIterator<QString> &i,
                           QTextCharFormat &textCharFormat,
                           QTextCharFormat const &defaultTextCharFormat);
  void updateButtons(bool enable);
};

#endif // MAINWINDOW_H
