#ifndef UPDATERWINDOW_H
#define UPDATERWINDOW_H

#include <QDialog>

namespace Ui {
class updaterwindow;
}
class BinarymodeWindow;
class SourcemodeWindow;

class updaterwindow : public QDialog {
  Q_OBJECT

public:
  explicit updaterwindow(QWidget *parent = nullptr);
  ~updaterwindow();

private:
  Ui::updaterwindow *ui;
  BinarymodeWindow *window_binarymode = nullptr;
  SourcemodeWindow *window_sourcemode = nullptr;

  void doUpdate();
  void startWindow();
  void cathookOS();
};

#endif // UPDATERWINDOW_H
