#include "mapselector.h"
#include "QListWidgetItem"
#include "ui_mapselector.h"
#include <QDir>
#include <QFile>
#include <QMessageBox>
#include <QTextStream>
#include <iostream>

struct TF2Map {
  int idindex;
  int listindex;
  QString mapname;
};

// clang-format off
QVector<TF2Map> TF2MapList {
    {1, 0, "cp_gorge"},
    {2, 0, "cp_badlands"},
    {3, 0, "cp_vanguard"},
    {4, 0, "cp_granary"},
    {5, 0, "cp_foundry"},
    {6, 0, "cp_gullywash_final1"},
    {7, 0, "cp_snakewater_final1"},
    {8, 0, "koth_viaduct"},
    {9, 0, "cp_gravelpit"},
    {10, 0, "cp_dustbowl"},
    {11, 0, "cp_well"},
    {12, 0, "ctf_2fort"},
    {13, 0, "tc_hydro"},
    {14, 0, "ctf_well"},
    {15, 0, "pl_goldrush"},
    {16, 0, "cp_fastlane"},
    {17, 0, "ctf_turbine"},
    {18, 0, "pl_badwater"},
    {19, 0, "cp_steel"},
    {20, 0, "cp_egypt_final"},
    {21, 0, "cp_junction_final"},
    {22, 0, "plr_pipeline"},
    {23, 0, "pl_hoodoo_final"},
    {24, 0, "koth_sawmill"},
    {25, 0, "koth_nucleus"},
    {26, 0, "ctf_sawmill"},
    {27, 0, "cp_yukon_final"},
    {28, 0, "koth_harvest_final"},
    {29, 0, "ctf_doublecross"},
    {30, 0, "cp_fright_final1"},
    {31, 0, "pl_upward"},
    {0, 1, "plr_hightower"},
    {1, 1, "pl_thundermountain"},
    {2, 1, "cp_coldfront"},
    {3, 1, "cp_mountainlab"},
    {4, 1, "cp_degrootkeep"},
    {5, 1, "cp_5gorge"},
    {6, 1, "pl_frontier_final"},
    {7, 1, "plr_nightfall_final"},
    {8, 1, "koth_lakeside_final"},
    {9, 1, "koth_badlands"},
    {10, 1, "pl_barnblitz"},
    {11, 1, "sd_doomsday"},
    {12, 1, "koth_king"},
    {13, 1, "cp_standin_final"},
    {14, 1, "cp_process_final"},
    {15, 1, "cp_sunshine"},
    {16, 1, "cp_metalworks"},
    {17, 1, "pl_swiftwater_final1"},
    {18, 1, "pass_brickyard"},
    {19, 1, "pass_timertown"},
    {20, 1, "pass_district"},
    {21, 1, "ctf_landfall"},
    {22, 1, "cp_snowplow"},
    {23, 1, "ctf_2fort_invasion"},
    {24, 1, "cp_powerhouse"},
    {25, 1, "koth_suijin"},
    {26, 1, "koth_probed"},
    {27, 1, "koth_highpass"},
    {29, 1, "pl_borneo"},
    {30, 1, "pl_snowycoast"},
    {31, 1, "pd_watergate"},
    {21, 2, "ctf_foundry"},
    {22, 2, "ctf_gorge"},
    {23, 2, "ctf_hellfire"},
    {24, 2, "ctf_thundermountain"},
    {5, 3, "cp_mercenarypark"},
    {6, 3, "cp_mossrock"},
    {7, 3, "koth_lazarus"},
    {8, 3, "plr_bananabay"},
    {9, 3, "pl_enclosure_final"},
    {10, 3, "koth_brazil"}
};
// clang-format on

auto path =
    QDir::homePath() +
    "/.steam/steam/steamapps/common/Team Fortress 2/tf/casual_criteria.vdf";

// map list id things, parse and apply to checkboxes
MapSelector::MapSelector(QWidget *parent)
    : QDialog(parent), ui(new Ui::MapSelector) {

  ui->setupUi(this);
  for (int i = 0; i < TF2MapList.size(); i++) {
    QListWidgetItem *item =
        new QListWidgetItem(TF2MapList[i].mapname, ui->mapList);
    item->setFlags(item->flags() | Qt::ItemIsUserCheckable);
    item->setCheckState(Qt::Checked);
    item->setData(Qt::UserRole, i);
  }
  ui->mapList->sortItems();
  Load();
}

MapSelector::~MapSelector() { delete ui; }

const static QString map_base_string("selected_maps_bits: ");

void MapSelector::Load() {
  auto criteria_file = QFile(path);
  if (!criteria_file.open(QIODevice::ReadOnly | QIODevice::Text))
    return;
  QTextStream in(&criteria_file);

  int list_index = 0;
  while (!in.atEnd()) {
    QString line = criteria_file.readLine();
    auto list = line.split(" ");

    if (list.size() == 2) {
      auto bitmask = list[1].toInt();
      for (int i = 0; i < ui->mapList->count(); i++) {
        auto uientry = ui->mapList->item(i);
        auto &maplistentry = TF2MapList[uientry->data(Qt::UserRole).toInt()];
        if (maplistentry.listindex != list_index)
          continue;

        if (bitmask & (1 << maplistentry.idindex))
          uientry->setCheckState(Qt::Checked);
        else
          uientry->setCheckState(Qt::Unchecked);
      }
    }
    list_index++;
  }
  criteria_file.close();
}

void MapSelector::Save() {
  std::array<int, 4> mapbitmasks{};
  for (int i = 0; i < ui->mapList->count(); i++) {
    auto uientry = ui->mapList->item(i);
    bool checked = uientry->checkState();
    if (checked) {
      auto &map = TF2MapList[uientry->data(Qt::UserRole).toInt()];
      mapbitmasks[map.listindex] |= 1 << map.idindex;
    }
  }

  QString write_string;
  for (auto mapbitmask : mapbitmasks)
    write_string += map_base_string + QString::number(mapbitmask) + "\n";

  auto criteria_file = QFile(path);
  if (!criteria_file.open(QIODevice::WriteOnly)) {
    QMessageBox::critical(this, "Error!",
                          "Unable to save the casual criteria file.");
    return;
  }
  criteria_file.write(write_string.toUtf8());
  criteria_file.close();
}

void MapSelector::on_buttonBox_accepted() { Save(); }
