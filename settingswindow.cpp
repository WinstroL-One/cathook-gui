#include "settingswindow.h"
#include "ui_settingswindow.h"
#include <QSettings>

SettingsWindow::SettingsWindow(QWidget *parent)
    : QDialog(parent), ui(new Ui::SettingsWindow) {
  ui->setupUi(this);
  settings = new QSettings("settings");
  ui->modeComboBox->setCurrentIndex(
      settings->value("cathook-gui/mode", 0).toInt());
}

SettingsWindow::~SettingsWindow() {
  delete ui;
  delete settings;
}

void SettingsWindow::on_buttonBox_accepted() {
  settings->setValue("cathook-gui/mode", ui->modeComboBox->currentIndex());
}
