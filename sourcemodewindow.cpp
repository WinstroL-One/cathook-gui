#include "sourcemodewindow.h"
#include "mapselector.h"
#include "settingswindow.h"
#include "ui_sourcemodewindow.h"
#include <QDesktopWidget>
#include <QDir>
#include <QFontDatabase>
#include <QProcess>
#include <QScrollBar>
#include <QStyle>
#include <csignal>
#include <iostream>

const QString terminals[] = {"konsole -e", "gnome-terminal -e",
                             "xfce4-terminal -e", "xterm -geometry 120x30+0+0"};

// Beginning https://stackoverflow.com/a/26524823
// based on information: http://en.m.wikipedia.org/wiki/ANSI_escape_code
// http://misc.flogisoft.com/bash/tip_colors_and_formatting
// http://invisible-island.net/xterm/ctlseqs/ctlseqs.html
void SourcemodeWindow::parseEscapeSequence(
    int attribute, QListIterator<QString> &i, QTextCharFormat &textCharFormat,
    QTextCharFormat const &defaultTextCharFormat) {
  switch (attribute) {
  case 0: { // Normal/Default (reset all attributes)
    textCharFormat = defaultTextCharFormat;
    break;
  }
  case 1: { // Bold/Bright (bold or increased intensity)
    textCharFormat.setFontWeight(QFont::Bold);
    break;
  }
  case 2: { // Dim/Faint (decreased intensity)
    textCharFormat.setFontWeight(QFont::Light);
    break;
  }
  case 3: { // Italicized (italic on)
    textCharFormat.setFontItalic(true);
    break;
  }
  case 4: { // Underscore (single underlined)
    textCharFormat.setUnderlineStyle(QTextCharFormat::SingleUnderline);
    textCharFormat.setFontUnderline(true);
    break;
  }
  case 5: { // Blink (slow, appears as Bold)
    textCharFormat.setFontWeight(QFont::Bold);
    break;
  }
  case 6: { // Blink (rapid, appears as very Bold)
    textCharFormat.setFontWeight(QFont::Black);
    break;
  }
  case 7: { // Reverse/Inverse (swap foreground and background)
    QBrush foregroundBrush = textCharFormat.foreground();
    textCharFormat.setForeground(textCharFormat.background());
    textCharFormat.setBackground(foregroundBrush);
    break;
  }
  case 8: { // Concealed/Hidden/Invisible (usefull for passwords)
    textCharFormat.setForeground(textCharFormat.background());
    break;
  }
  case 9: { // Crossed-out characters
    textCharFormat.setFontStrikeOut(true);
    break;
  }
  case 10: { // Primary (default) font
    textCharFormat.setFont(defaultTextCharFormat.font());
    break;
  }
  case 11 ... 19: {
    QFontDatabase fontDatabase;
    QString fontFamily = textCharFormat.fontFamily();
    QStringList fontStyles = fontDatabase.styles(fontFamily);
    int fontStyleIndex = attribute - 11;
    if (fontStyleIndex < fontStyles.length()) {
      textCharFormat.setFont(
          fontDatabase.font(fontFamily, fontStyles.at(fontStyleIndex),
                            textCharFormat.font().pointSize()));
    }
    break;
  }
  case 20: { // Fraktur (unsupported)
    break;
  }
  case 21: { // Set Bold off
    textCharFormat.setFontWeight(QFont::Normal);
    break;
  }
  case 22: { // Set Dim off
    textCharFormat.setFontWeight(QFont::Normal);
    break;
  }
  case 23: { // Unset italic and unset fraktur
    textCharFormat.setFontItalic(false);
    break;
  }
  case 24: { // Unset underlining
    textCharFormat.setUnderlineStyle(QTextCharFormat::NoUnderline);
    textCharFormat.setFontUnderline(false);
    break;
  }
  case 25: { // Unset Blink/Bold
    textCharFormat.setFontWeight(QFont::Normal);
    break;
  }
  case 26: { // Reserved
    break;
  }
  case 27: { // Positive (non-inverted)
    QBrush backgroundBrush = textCharFormat.background();
    textCharFormat.setBackground(textCharFormat.foreground());
    textCharFormat.setForeground(backgroundBrush);
    break;
  }
  case 28: {
    textCharFormat.setForeground(defaultTextCharFormat.foreground());
    textCharFormat.setBackground(defaultTextCharFormat.background());
    break;
  }
  case 29: {
    textCharFormat.setUnderlineStyle(QTextCharFormat::NoUnderline);
    textCharFormat.setFontUnderline(false);
    break;
  }
  case 30 ... 37: {
    int colorIndex = attribute - 30;
    QColor color;
    if (QFont::Normal < textCharFormat.fontWeight()) {
      switch (colorIndex) {
      case 0: {
        color = Qt::darkGray;
        break;
      }
      case 1: {
        color = Qt::red;
        break;
      }
      case 2: {
        color = Qt::green;
        break;
      }
      case 3: {
        color = Qt::yellow;
        break;
      }
      case 4: {
        color = Qt::blue;
        break;
      }
      case 5: {
        color = Qt::magenta;
        break;
      }
      case 6: {
        color = Qt::cyan;
        break;
      }
      case 7: {
        color = Qt::white;
        break;
      }
      default: {
        Q_ASSERT(false);
      }
      }
    } else {
      switch (colorIndex) {
      case 0: {
        color = Qt::black;
        break;
      }
      case 1: {
        color = Qt::darkRed;
        break;
      }
      case 2: {
        color = Qt::darkGreen;
        break;
      }
      case 3: {
        color = Qt::darkYellow;
        break;
      }
      case 4: {
        color = Qt::darkBlue;
        break;
      }
      case 5: {
        color = Qt::darkMagenta;
        break;
      }
      case 6: {
        color = Qt::darkCyan;
        break;
      }
      case 7: {
        color = Qt::lightGray;
        break;
      }
      default: {
        Q_ASSERT(false);
      }
      }
    }
    textCharFormat.setForeground(color);
    break;
  }
  case 38: {
    if (i.hasNext()) {
      bool ok = false;
      int selector = i.next().toInt(&ok);
      Q_ASSERT(ok);
      QColor color;
      switch (selector) {
      case 2: {
        if (!i.hasNext()) {
          break;
        }
        int red = i.next().toInt(&ok);
        Q_ASSERT(ok);
        if (!i.hasNext()) {
          break;
        }
        int green = i.next().toInt(&ok);
        Q_ASSERT(ok);
        if (!i.hasNext()) {
          break;
        }
        int blue = i.next().toInt(&ok);
        Q_ASSERT(ok);
        color.setRgb(red, green, blue);
        break;
      }
      case 5: {
        if (!i.hasNext()) {
          break;
        }
        int index = i.next().toInt(&ok);
        Q_ASSERT(ok);
        switch (index) {
        case 0x00 ... 0x07: { // 0x00-0x07:  standard colors (as in ESC [ 30..37
                              // m)
          return parseEscapeSequence(index - 0x00 + 30, i, textCharFormat,
                                     defaultTextCharFormat);
        }
        case 0x08 ... 0x0F: { // 0x08-0x0F:  high intensity colors (as in ESC
                              // [ 90..97 m)
          return parseEscapeSequence(index - 0x08 + 90, i, textCharFormat,
                                     defaultTextCharFormat);
        }
        case 0x10 ... 0xE7: { // 0x10-0xE7:  6*6*6=216 colors: 16 + 36*r + 6*g +
                              // b (0≤r,g,b≤5)
          index -= 0x10;
          int red = index % 6;
          index /= 6;
          int green = index % 6;
          index /= 6;
          int blue = index % 6;
          index /= 6;
          Q_ASSERT(index == 0);
          color.setRgb(red, green, blue);
          break;
        }
        case 0xE8 ... 0xFF: { // 0xE8-0xFF:  grayscale from black to white in 24
                              // steps
          qreal intensity = qreal(index - 0xE8) / (0xFF - 0xE8);
          color.setRgbF(intensity, intensity, intensity);
          break;
        }
        }
        textCharFormat.setForeground(color);
        break;
      }
      default: {
        break;
      }
      }
    }
    break;
  }
  case 39: {
    textCharFormat.setForeground(defaultTextCharFormat.foreground());
    break;
  }
  case 40 ... 47: {
    int colorIndex = attribute - 40;
    QColor color;
    switch (colorIndex) {
    case 0: {
      color = Qt::darkGray;
      break;
    }
    case 1: {
      color = Qt::red;
      break;
    }
    case 2: {
      color = Qt::green;
      break;
    }
    case 3: {
      color = Qt::yellow;
      break;
    }
    case 4: {
      color = Qt::blue;
      break;
    }
    case 5: {
      color = Qt::magenta;
      break;
    }
    case 6: {
      color = Qt::cyan;
      break;
    }
    case 7: {
      color = Qt::white;
      break;
    }
    default: {
      Q_ASSERT(false);
    }
    }
    textCharFormat.setBackground(color);
    break;
  }
  case 48: {
    if (i.hasNext()) {
      bool ok = false;
      int selector = i.next().toInt(&ok);
      Q_ASSERT(ok);
      QColor color;
      switch (selector) {
      case 2: {
        if (!i.hasNext()) {
          break;
        }
        int red = i.next().toInt(&ok);
        Q_ASSERT(ok);
        if (!i.hasNext()) {
          break;
        }
        int green = i.next().toInt(&ok);
        Q_ASSERT(ok);
        if (!i.hasNext()) {
          break;
        }
        int blue = i.next().toInt(&ok);
        Q_ASSERT(ok);
        color.setRgb(red, green, blue);
        break;
      }
      case 5: {
        if (!i.hasNext()) {
          break;
        }
        int index = i.next().toInt(&ok);
        Q_ASSERT(ok);
        switch (index) {
        case 0x00 ... 0x07: { // 0x00-0x07:  standard colors (as in ESC [ 40..47
                              // m)
          return parseEscapeSequence(index - 0x00 + 40, i, textCharFormat,
                                     defaultTextCharFormat);
        }
        case 0x08 ... 0x0F: { // 0x08-0x0F:  high intensity colors (as in ESC [
                              // 100..107 m)
          return parseEscapeSequence(index - 0x08 + 100, i, textCharFormat,
                                     defaultTextCharFormat);
        }
        case 0x10 ... 0xE7: { // 0x10-0xE7:  6*6*6=216 colors: 16 + 36*r + 6*g +
                              // b (0≤r,g,b≤5)
          index -= 0x10;
          int red = index % 6;
          index /= 6;
          int green = index % 6;
          index /= 6;
          int blue = index % 6;
          index /= 6;
          Q_ASSERT(index == 0);
          color.setRgb(red, green, blue);
          break;
        }
        case 0xE8 ... 0xFF: { // 0xE8-0xFF:  grayscale from black to white in 24
                              // steps
          qreal intensity = qreal(index - 0xE8) / (0xFF - 0xE8);
          color.setRgbF(intensity, intensity, intensity);
          break;
        }
        }
        textCharFormat.setBackground(color);
        break;
      }
      default: {
        break;
      }
      }
    }
    break;
  }
  case 49: {
    textCharFormat.setBackground(defaultTextCharFormat.background());
    break;
  }
  case 90 ... 97: {
    int colorIndex = attribute - 90;
    QColor color;
    switch (colorIndex) {
    case 0: {
      color = Qt::darkGray;
      break;
    }
    case 1: {
      color = Qt::red;
      break;
    }
    case 2: {
      color = Qt::green;
      break;
    }
    case 3: {
      color = Qt::yellow;
      break;
    }
    case 4: {
      color = Qt::blue;
      break;
    }
    case 5: {
      color = Qt::magenta;
      break;
    }
    case 6: {
      color = Qt::cyan;
      break;
    }
    case 7: {
      color = Qt::white;
      break;
    }
    default: {
      Q_ASSERT(false);
    }
    }
    color.setRedF(color.redF() * 0.8);
    color.setGreenF(color.greenF() * 0.8);
    color.setBlueF(color.blueF() * 0.8);
    textCharFormat.setForeground(color);
    break;
  }
  case 100 ... 107: {
    int colorIndex = attribute - 100;
    QColor color;
    switch (colorIndex) {
    case 0: {
      color = Qt::darkGray;
      break;
    }
    case 1: {
      color = Qt::red;
      break;
    }
    case 2: {
      color = Qt::green;
      break;
    }
    case 3: {
      color = Qt::yellow;
      break;
    }
    case 4: {
      color = Qt::blue;
      break;
    }
    case 5: {
      color = Qt::magenta;
      break;
    }
    case 6: {
      color = Qt::cyan;
      break;
    }
    case 7: {
      color = Qt::white;
      break;
    }
    default: {
      Q_ASSERT(false);
    }
    }
    color.setRedF(color.redF() * 0.8);
    color.setGreenF(color.greenF() * 0.8);
    color.setBlueF(color.blueF() * 0.8);
    textCharFormat.setBackground(color);
    break;
  }
  default: {
    break;
  }
  }
}

void SourcemodeWindow::setTextTermFormatting(QTextEdit *textEdit,
                                             QString const &text) {
  QTextDocument *document = textEdit->document();
  QRegExp const escapeSequenceExpression(R"(\x1B\[([\d;]+)m)");
  QTextCursor cursor(document);
  QTextCharFormat const defaultTextCharFormat = cursor.charFormat();
  cursor.beginEditBlock();
  int offset = escapeSequenceExpression.indexIn(text);
  cursor.insertText(text.mid(0, offset));
  QTextCharFormat textCharFormat = defaultTextCharFormat;
  while (!(offset < 0)) {
    int previousOffset = offset + escapeSequenceExpression.matchedLength();
    QStringList capturedTexts =
        escapeSequenceExpression.capturedTexts().back().split(';');
    QListIterator<QString> i(capturedTexts);
    while (i.hasNext()) {
      bool ok = false;
      int attribute = i.next().toInt(&ok);
      Q_ASSERT(ok);
      parseEscapeSequence(attribute, i, textCharFormat, defaultTextCharFormat);
    }
    offset = escapeSequenceExpression.indexIn(text, previousOffset);
    if (offset < 0) {
      cursor.insertText(text.mid(previousOffset), textCharFormat);
    } else {
      cursor.insertText(text.mid(previousOffset, offset - previousOffset),
                        textCharFormat);
    }
  }
  cursor.setCharFormat(defaultTextCharFormat);
  cursor.endEditBlock();
  // cursor.movePosition(QTextCursor::Start);
  textEdit->setTextCursor(cursor);
}
// End https://stackoverflow.com/a/26524823

SourcemodeWindow::SourcemodeWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow) {
  ui->setupUi(this);
  setGeometry(QStyle::alignedRect(Qt::LeftToRight, Qt::AlignCenter, size(),
                                  qApp->desktop()->availableGeometry()));
  updateButtons(true);
}

SourcemodeWindow::~SourcemodeWindow() {
  if (process && process->state() != process->NotRunning) {
    kill(process->pid(), SIGINT);
    bool stopped = process->waitForFinished(5000);
    if (!stopped) {
      process->terminate();
      stopped = process->waitForFinished(5000);
      if (!stopped && process->state() != process->NotRunning) {
        process->kill();
        process->waitForFinished();
      }
    }
  }
  process.reset();
  delete ui;
}

void SourcemodeWindow::printTerminal(QString data) {
  auto text = ui->terminalOutput->toPlainText();
  ui->terminalOutput->setUpdatesEnabled(false);
  ui->terminalOutput->clear();
  setTextTermFormatting(ui->terminalOutput, text + data);
  QScrollBar *sb = ui->terminalOutput->verticalScrollBar();
  sb->setValue(sb->maximum());
  ui->terminalOutput->setUpdatesEnabled(true);
  // Output the data
  std::cout << data.toStdString();
  std::cout.flush();
}

void SourcemodeWindow::readTerminalOutput() {
  // Continue reading the data until EOF reached
  QByteArray data = this->process->readAll();
  printTerminal(data);
}

void SourcemodeWindow::closeTerminalProcess() {
  updateButtons(true);
  process.reset();
};

void SourcemodeWindow::updateButtons(bool enable) {
  if (enable) {
    bool cathookDirExists = QDir("cathook").exists();
    if (cathookDirExists) {
      ui->updateButton->setText("Update");
      ui->updateButton->setEnabled(true);
    } else {
      ui->updateButton->setText("Install");
      ui->updateButton->setEnabled(true);
    }
    ui->injectButton->setEnabled(cathookDirExists);
    ui->configButton->setEnabled(cathookDirExists);
    ui->preloadButton->setEnabled(cathookDirExists);

    ui->menuBar->setEnabled(true);
  } else {
    ui->updateButton->setEnabled(false);
    ui->injectButton->setEnabled(false);
    ui->configButton->setEnabled(false);
    ui->preloadButton->setEnabled(false);
    ui->menuBar->setEnabled(false);
  }
}

void SourcemodeWindow::on_updateButton_clicked() {
  updateButtons(false);
  ui->terminalOutput->clear();
  process = std::make_unique<QProcess>();
  process->setProcessChannelMode(QProcess::MergedChannels);
  if (QDir("cathook").exists())
    process->start(
        "bash -c \"cd cathook;CH_SUDO=pkexec CH_DEPENDENCYCHECK_FORCEYES=true "
        "./scripts/updater false true\"");
  else
    process->start(
        std::string(
            "bash -c \"CH_SUDO=pkexec CH_DEPENDENCYCHECK_FORCEYES=true bash "
            "<(wget -qO- "
            "https://raw.githubusercontent.com/nullworks/"
            "One-in-all-cathook-install/master/install-all); echo Done\"")
            .data());

  // Wait for it to start
  if (!process->waitForStarted()) {
    updateButtons(true);
    return;
  }

  connect(process.get(), SIGNAL(readyReadStandardOutput()), this,
          SLOT(readTerminalOutput()));
  connect(process.get(), SIGNAL(finished(int, QProcess::ExitStatus)), this,
          SLOT(closeTerminalProcess()));
}

void SourcemodeWindow::on_injectButton_clicked() {
  if (!QDir("cathook").exists()) {
    updateButtons(true);
    return;
  }
  updateButtons(false);
  ui->terminalOutput->clear();
  process = std::make_unique<QProcess>();
  process->setProcessChannelMode(QProcess::MergedChannels);
  process->start("bash -c \"pkexec bash -c 'cd " + QDir::currentPath() +
                 "/cathook; SUDO_USER=" + qgetenv("USER") + " ./attach'\"");

  // Wait for it to start
  if (!this->process->waitForStarted()) {
    updateButtons(true);
    return;
  }

  connect(process.get(), SIGNAL(readyReadStandardOutput()), this,
          SLOT(readTerminalOutput()));
  connect(process.get(), SIGNAL(finished(int, QProcess::ExitStatus)), this,
          SLOT(closeTerminalProcess()));
}

void SourcemodeWindow::on_configButton_clicked() {
  if (!QDir("cathook").exists()) {
    updateButtons(true);
    return;
  }
  updateButtons(false);
  for (auto term : terminals) {
    process = std::make_unique<QProcess>();
    process->setProcessChannelMode(QProcess::MergedChannels);
    process->setWorkingDirectory("cathook");
    auto str = term + " ./config";

    process->start(str);
    if (!process->waitForStarted(2000)) {
      printTerminal("Couldn't start termial process via \"" + str + "\".");
      closeTerminalProcess();
      continue;
    }
    break;
  }
  ui->terminalOutput->clear();
  connect(process.get(), SIGNAL(finished(int, QProcess::ExitStatus)), this,
          SLOT(closeTerminalProcess()));
}

void SourcemodeWindow::on_preloadButton_clicked() {
  if (!QDir("cathook").exists()) {
    updateButtons(true);
    return;
  }
  updateButtons(false);
  ui->terminalOutput->clear();
  process = std::make_unique<QProcess>();
  process->setProcessChannelMode(QProcess::MergedChannels);
  process->start("bash -c \"cd cathook; ./preload\"");

  // Wait for it to start
  if (!this->process->waitForStarted()) {
    updateButtons(true);
    return;
  }

  connect(process.get(), SIGNAL(readyReadStandardOutput()), this,
          SLOT(readTerminalOutput()));
  connect(process.get(), SIGNAL(finished(int, QProcess::ExitStatus)), this,
          SLOT(closeTerminalProcess()));
}

void SourcemodeWindow::on_actionSelect_Maps_triggered() {
  updateButtons(false);
  MapSelector selector;
  selector.exec();
  updateButtons(true);
}

void SourcemodeWindow::on_actionSettings_triggered() {
  updateButtons(false);
  SettingsWindow settings;
  settings.exec();
  updateButtons(true);
}
