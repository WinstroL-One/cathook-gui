#ifndef MAPSELECTOR_H
#define MAPSELECTOR_H

#include <QDialog>

namespace Ui {
class MapSelector;
}

class MapSelector : public QDialog {
  Q_OBJECT

public:
  explicit MapSelector(QWidget *parent = nullptr);
  ~MapSelector();

private slots:
  void on_buttonBox_accepted();

private:
  Ui::MapSelector *ui;
  void Save();
  void Load();
};

#endif // MAPSELECTOR_H
