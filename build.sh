set -e
rm -r build | true
mkdir -p build && cp Cathook-GUI.desktop ./build/ && cp cathook.png ./build/Cathook-GUI.png && cd build
qmake .. && make
../tools/linuxdeploy-x86_64.AppImage --plugin qt --executable=./Cathook-GUI --appdir=./appdir/ --desktop-file ./Cathook-GUI.desktop --icon-file ./Cathook-GUI.png
../tools/appimageupdatetool-x86_64.AppImage --appimage-extract
cp -r ./squashfs-root/ ./appdir/usr/bin/appimageupdatetool
rm -r ./appdir/usr/translations | true
rm -r ./appdir/usr/doc | true
rm -r ./appdir/usr/bin/appimageupdatetool/resources | true
rm -r ./appdir/usr/bin/appimageupdatetool/usr/share/icons | true
rm -r ./appdir/usr/bin/appimageupdatetool/usr/share/doc | true
rm -r ./squashfs-root | true
\cp --remove-destination ./appdir/usr/share/applications/Cathook-GUI.desktop ./appdir
UPDATE_INFORMATION="zsync|https://cathook.club/s/ch/guiupdater/Cathook-GUI-x86_64.AppImage.zsync" ../tools/linuxdeploy-plugin-appimage-x86_64.AppImage --appdir ./appdir
