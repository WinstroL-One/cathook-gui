#ifndef BINARYMODEWINDOW_H
#define BINARYMODEWINDOW_H

#include "QtNetwork/QNetworkAccessManager"
#include <QMainWindow>

namespace Ui {
class BinarymodeWindow;
}

class BinarymodeWindow : public QMainWindow {
  Q_OBJECT

public:
    explicit BinarymodeWindow(QWidget *parent = nullptr, bool archlinux = false);
  ~BinarymodeWindow();

private slots:
  void on_attachButton_clicked();
  // Not typical Qt slot naming style, cause that causes a warning
  void onPackageDownloadFinished();

  void on_actionSelect_Maps_triggered();
  void on_actionSettings_triggered();

private:
  Ui::BinarymodeWindow *ui;
  QString datapath;
  QNetworkAccessManager network_manager;
  QNetworkReply *network_reply;
  bool archlinux;

  void downloadPackage(bool gdb);
  void updateButtons(bool enable);
};

#endif // BINARYMODEWINDOW_H
