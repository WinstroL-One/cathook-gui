#ifndef SETTINGS_H
#define SETTINGS_H

#include <QDialog>

namespace Ui {
class SettingsWindow;
}

class QSettings;

class SettingsWindow : public QDialog {
  Q_OBJECT

public:
  explicit SettingsWindow(QWidget *parent = nullptr);
  ~SettingsWindow();

private slots:
  void on_buttonBox_accepted();

private:
  Ui::SettingsWindow *ui;
  QSettings *settings;
};

#endif // SETTINGS_H
