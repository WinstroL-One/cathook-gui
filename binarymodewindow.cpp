#include "mapselector.h"
#include "settingswindow.h"
#include "ui_binarymodewindow.h"
#include <QDesktopWidget>
#include <QDir>
#include <QMessageBox>
#include <QProcess>
#include <QStandardPaths>
#include <QStyle>
#include <QtNetwork/QNetworkReply>
#include <QtNetwork/QNetworkRequest>
#include <binarymodewindow.h>

auto link_packagefile =
    QUrl("https://nullworks.gitlab.io/cathook/cathook/package.tar.gz");
auto link_gdb = QUrl("https://nullworks.gitlab.io/cathook/cathook/gdb");

BinarymodeWindow::BinarymodeWindow(QWidget *parent, bool archlinux)
    : QMainWindow(parent), ui(new Ui::BinarymodeWindow), archlinux(archlinux) {
  datapath = QStandardPaths::writableLocation(QStandardPaths::DataLocation);
  ui->setupUi(this);
  setGeometry(QStyle::alignedRect(Qt::LeftToRight, Qt::AlignCenter, size(),
                                  qApp->desktop()->availableGeometry()));
}

BinarymodeWindow::~BinarymodeWindow() { delete ui; }

void BinarymodeWindow::updateButtons(bool enable) {
  ui->attachButton->setEnabled(enable);
  ui->menuBar->setEnabled(enable);
}

void BinarymodeWindow::downloadPackage(bool gdb) {
  network_reply = network_manager.get(QNetworkRequest(link_packagefile));
  connect(network_reply, SIGNAL(finished()), this,
          SLOT(onPackageDownloadFinished()));
  connect(network_reply, &QNetworkReply::downloadProgress,
          [=](qint64 downloaded, qint64 total) {
            ui->progressBar->setValue(
                float(downloaded) / total * (!gdb ? 80 : 40) + (gdb ? 40 : 0));
          });
}

void BinarymodeWindow::on_attachButton_clicked() {
  updateButtons(false);
  if (!QDir(datapath).exists()) {
    QDir().mkdir(datapath);
  }
  ui->statusText->setText("Downloading...");
  // Check if the system has gdb
  bool hasgdb = !archlinux && (QFile("/bin/gdb").exists() || QFile("/usr/bin/gdb").exists());
  if (!hasgdb) {
    auto gdb = network_manager.get(QNetworkRequest(link_gdb));

    connect(gdb, &QNetworkReply::downloadProgress,
            [=](qint64 downloaded, qint64 total) {
              ui->progressBar->setValue(float(downloaded) / total * 40);
            });
    connect(gdb, &QNetworkReply::finished, [=]() {
      bool download_success = gdb->error() == network_reply->NoError;
      if (download_success) {
        QByteArray data = gdb->readAll();
        QFile file = QFile(datapath + "/gdb");
        file.open(QIODevice::WriteOnly);
        file.write(data);
        file.close();
        // If we couldn't download the file and it doesn't exist yet, tell the
        // user
      } else if (!QFile(datapath + "/gdb").exists()) {
        ui->statusText->setText("Download failed!");
        ui->progressBar->setValue(100);
        QMessageBox::critical(this, "Download failed!",
                              "Download of the gdb files failed! Make sure "
                              "you are connected to the internet!");
        updateButtons(true);
        return;
      } else
        ui->progressBar->setValue(40);
      gdb->deleteLater();
      downloadPackage(true);
    });
  } else {
    downloadPackage(false);
  }
}

void BinarymodeWindow::onPackageDownloadFinished() {
  bool download_success = network_reply->error() == network_reply->NoError;
  if (download_success) {
    QByteArray data = network_reply->readAll();
    QFile file = QFile(datapath + "/package.tar.gz");
    file.open(QIODevice::WriteOnly);
    file.write(data);
    file.close();
    // If we couldn't download the file and it doesn't exist yet, tell the user
  } else if (!QFile(datapath + "/package.tar.gz").exists()) {
    ui->statusText->setText("Download failed!");
    ui->progressBar->setValue(100);
    QMessageBox::critical(this, "Download failed!",
                          "Download of the cathook files failed! Make sure "
                          "you are connected to the internet!");
    updateButtons(true);
    return;
  } else
    // file already exists, tell the user
    ui->progressBar->setValue(80);
  // Free memory for the download
  network_reply->deleteLater();
  ui->statusText->setText("Installing...");
  // Remove existing package directory to prepare for for installation
  QDir().remove(datapath + "/package");

  // Create QProcess on heap so it can outlast the scope of the stack
  QProcess *install = new QProcess();
  install->setWorkingDirectory(datapath);
  // Start tar process and unpack the package
  install->start(
      "bash -c \"tar -xf package.tar.gz && cd package && ./install\"");

  // Wait for the install process to finish
  connect(
      install, QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished),
      [=](int, QProcess::ExitStatus) {
        // Free the heap allocated QProcess
        install->deleteLater();
        ui->progressBar->setValue(90);
        ui->statusText->setText("Attaching...");

        QProcess *attach = new QProcess();
        attach->start("bash -c \"pkexec bash -c 'cd " + datapath +
                      "/package; SUDO_USER=" + qgetenv("USER") +
                      " ./attach'\"");
        connect(attach,
                QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished),
                [=](int exitCode, QProcess::ExitStatus) {
                  attach->deleteLater();
                  ui->progressBar->setValue(100);
                  if (exitCode != 0) {
                    ui->statusText->setText("Failed!");
                    switch (exitCode) {
                    case 2:
                      QMessageBox::critical(this, "Attaching failed!",
                                            "TF2 is not running!");
                      break;
                    default:
                      QMessageBox::critical(
                          this, "Attaching failed!",
                          "Attaching failed because of an unknown error.");
                    }

                  } else
                    ui->statusText->setText("Done!");
                  // We're done here. Delete the package directory
                  QDir().remove(datapath + "/package");
                  updateButtons(true);
                });
      });
}

void BinarymodeWindow::on_actionSelect_Maps_triggered() {
  updateButtons(false);
  MapSelector selector;
  selector.exec();
  updateButtons(true);
}

void BinarymodeWindow::on_actionSettings_triggered() {
  updateButtons(false);
  SettingsWindow settings;
  settings.exec();
  updateButtons(true);
}
