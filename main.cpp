#include "updaterwindow.h"
#include <QApplication>
#include <QFile>
#include <QIcon>
#include <QSettings>
#include <QStandardPaths>
#include <QTextStream>

int main(int argc, char *argv[]) {
  QApplication a(argc, argv);
  a.setWindowIcon(QIcon(":/cathook.png"));
  a.setApplicationName("cathook");
  QSettings::setPath(
      QSettings::NativeFormat, QSettings::UserScope,
      QStandardPaths::writableLocation(QStandardPaths::DataLocation));

  QFile f(":qdarkstyle/style.qss");

  if (!f.exists()) {
    printf("Unable to set stylesheet, file not found\n");
  } else {
    f.open(QFile::ReadOnly | QFile::Text);
    QTextStream ts(&f);
    qApp->setStyleSheet(ts.readAll());
  }

  updaterwindow updater;

  return a.exec();
}
